package data.scripts.util;

interface StringCreator {
    String create();
}
